package com.corvu.api.service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.corvu.api.dto.JwtResponseDto;
import com.corvu.api.dto.MessageDto;
import com.corvu.api.dto.UserDto;
import com.corvu.api.entity.Role;
import com.corvu.api.entity.User;
import com.corvu.api.enums.RoleEnum;
import com.corvu.api.repository.RoleRepository;
import com.corvu.api.repository.UserRepository;
import com.corvu.api.security.UserDetailsImpl;
import com.corvu.api.security.jwt.JwtUtils;

@Component
public class AuthenticationService {

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtUtils jwtUtils;

	/**
	 * Authenticate a user if the credentials are known
	 * @param userDto
	 * @return A Jwt
	 */
	public ResponseEntity<?> signin(final UserDto userDto) {

		final Authentication authentication = authenticationManager.authenticate(
				new UsernamePasswordAuthenticationToken(userDto.getUsername(), userDto.getPassword()));

		SecurityContextHolder.getContext().setAuthentication(authentication);
		final String jwt = jwtUtils.generateJwtToken(authentication);

		final UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
		final List<String> roles = userDetails.getAuthorities().stream()
				.map(role -> role.getAuthority())
				.collect(Collectors.toList());

		return ResponseEntity.ok(new JwtResponseDto(
				jwt,
				userDetails.getId(),
				userDetails.getUsername(),
				userDetails.getEmail(),
				roles));
	}

	/**
	 * Signup a new user
	 * @param userDto
	 * @return A messageDto
	 */
	public ResponseEntity<?> signup(final UserDto userDto) {
		if (userRepository.existsByUsername(userDto.getUsername())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageDto("Error: Username is already taken!"));
		}

		if (userRepository.existsByEmail(userDto.getEmail())) {
			return ResponseEntity
					.badRequest()
					.body(new MessageDto("Error: Email is already in use!"));
		}

		// Create new user's account
		final User user = new User(userDto.getUsername(),
				passwordEncoder.encode(userDto.getPassword()),
				userDto.getEmail());

		final Set<String> strRoles = userDto.getRoles();
		final Set<Role> roles = new HashSet<>();

		if (strRoles == null) {
			final Role userRole = roleRepository.findByName(RoleEnum.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
			roles.add(userRole);
		} else {
			strRoles.forEach(role -> {
				switch (role) {
				case "admin":
					final Role adminRole = roleRepository.findByName(RoleEnum.ROLE_ADMIN)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(adminRole);

					break;
				default:
					final Role userRole = roleRepository.findByName(RoleEnum.ROLE_USER)
					.orElseThrow(() -> new RuntimeException("Error: Role is not found."));
					roles.add(userRole);
				}
			});
		}

		user.setRoles(roles);
		userRepository.save(user);

		return ResponseEntity.ok(new MessageDto("User registered successfully!"));
	}
}
