package com.corvu.api.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.corvu.api.entity.Test;

@Repository
public interface TestRepository extends JpaRepository<Test, Long> {

	Optional<Test> findById(Long id);
	
}
