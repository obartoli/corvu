package com.corvu.api.dto;

import java.util.Set;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;

public class UserDto {

	@Size(min = 3)
	private String username;

	@Email
	private String email;

	@Size(min = 6)
	private String password;

	private Set<String> roles;

	public String getUsername() {
		return username;
	}

	public void setUsername(final String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(final String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(final String password) {
		this.password = password;
	}

	public Set<String> getRoles() {
		return roles;
	}

	public void setRoles(final Set<String> roles) {
		this.roles = roles;
	}


}
