package com.corvu.api.dto;

public class MessageDto {

	private String message;

	public MessageDto(final String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(final String message) {
		this.message = message;
	}
}
