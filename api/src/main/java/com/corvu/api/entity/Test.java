package com.corvu.api.entity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Test {

	@Id
	Long id;
	
	public Long getId() {
		return this.id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
}
