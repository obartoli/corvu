package com.corvu.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorvuApplication {

	public static void main(final String[] args) {
		SpringApplication.run(CorvuApplication.class, args);
	}
}
