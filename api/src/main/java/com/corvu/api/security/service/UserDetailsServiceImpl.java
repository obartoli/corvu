package com.corvu.api.security.service;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.corvu.api.entity.User;
import com.corvu.api.repository.UserRepository;
import com.corvu.api.security.UserDetailsImpl;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	@Transactional
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		Objects.requireNonNull(username);
		final User user = this.userRepository.findByUsername(username).orElseThrow(
				() -> new UsernameNotFoundException("User not found with username " + username));
		return UserDetailsImpl.build(user);
	}
}
