import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const API_URL = 'http://localhost:8080/corvu/public/';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getTestContent(): Observable<any> {
    return this.http.get(API_URL + 'test', { responseType: 'text'});
  }
}
