import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { faUser, faLock } from '@fortawesome/free-solid-svg-icons';

import { AuthenticationService } from '../../services/authentication.service';
import { TokenStorageService } from '../../services/token-storage.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  credentials: any = { };
  roles: string[] = [];
  isLoggedIn: boolean = false;
  isLoginFailed: boolean = false;
  errorMessage: string = '';

  icons = { faUser: faUser, faLock: faLock };

  constructor(private authentication: AuthenticationService, private token: TokenStorageService, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
    if(this.token.getToken()) {
      this.isLoggedIn = true;
      this.roles = this.token.getUser().roles;
    }
  }

  ngOnSubmit(): void {    
    this.authentication.login(this.credentials).subscribe(
      data => {
        this.token.saveToken(data.accessToken);
        this.token.saveUser(data);
        
        this.roles = this.token.getUser().roles;
        this.isLoggedIn = true;
        this.isLoginFailed = false;

        this.router.navigateByUrl('/');
      },
      error => {
        this.errorMessage = error.error.message;
        this.isLoggedIn = false;
        this.isLoginFailed = true;
      }
    );
  }
}
