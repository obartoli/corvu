import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { faUser, faLock, faEnvelope } from '@fortawesome/free-solid-svg-icons';

import { AuthenticationService } from '../../services/authentication.service';
import { TokenStorageService } from '../../services/token-storage.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  credentials: any = { };

  icons = { faUser: faUser, faLock: faLock, faEnvelope: faEnvelope };

  constructor(private authentication: AuthenticationService, private token: TokenStorageService, private http: HttpClient, private router: Router) { }

  ngOnInit(): void {
  }

  ngOnSubmit(): void {
    
  }

}
